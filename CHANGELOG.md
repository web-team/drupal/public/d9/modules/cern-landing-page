Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [2.2.6] - 02/12/2021

- Removed deprecated call `getCurrentUserId` from `core.base_field_override.node.landing_page.uid.yml` configuration
- New call is `default_value_callback: 'Drupal\node\Entity\Node::getDefaultEntityOwner'`
## [2.2.5] - 25/11/2021

- Add `core_version_requirement: ^9 || ^10` and remove `core: 8.x` from composer.

## [2.2.4] - 08/02/2021

- Add core: 8.x to fix enabling issue

## [2.2.3] - 14/01/2021

- Fix D9 requirements

## [2.2.2] - 12/01/2021

- Update module to be d9-ready

## [2.2.1] - 07/12/2020

- Add composer.json file

## [2.2.0] - 21/01/2019

- Fix "Non-translatable fields can only be changed when updating the current revision." error
- Remove dependency with Content Translation module.

## [2.1.0] - 10/12/2018

- Add file structure
